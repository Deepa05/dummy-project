import {useLocation} from "react-router-dom";
import './InfoDisplay.css';


export default function InfoDisplay()
{

   let location = useLocation();

    return(
        <div>
            <h2>InfoDisplay Component</h2>
            <ul className="Info-List">
                <li><b>name :</b> {location.state.name}</li>
                <li><b>nasa_jpl_url :</b> {location.state.nasa_jpl_url}</li>
                {location.state.is_potentially_hazardous_asteroid ?
                <li><b>is_potentially_hazardous_asteroid :</b>True</li> :
                <li><b>is_potentially_hazardous_asteroid :</b>False</li>}
            </ul>   
        </div>
    )
}