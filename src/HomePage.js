import './HomePage.css';
import {TextField} from '@mui/material';
import {Button} from '@mui/material';
import { useState } from 'react';
import {useNavigate,Link} from 'react-router-dom';
import axios from 'axios';
import {makeStyles} from '@mui/styles';



const useStyles= makeStyles({
    root:{
        background: 'white',
        '& .MuiInputBase-root':{
            color:'yellow',
            background:'pink',
            margin: '1rem',
            
        },
        '& Button':{
            color:'darkslategrey',
            background:'pink',
            margin: '1rem',
            '&: hover':{
                background:'white'
            }
        }

    }
})

export default function HomePage(){

    const [UserId, setUserId]=useState("");
    const MY_KEY ="LHggyN4tjlmPedm8qwSgip9LEY5DqTJkm3UaOJ5w";
    let navigate = useNavigate();

    const classes = useStyles();


    function submitHandler(UserId){
      
        axios.get(`https://api.nasa.gov/neo/rest/v1/neo/${UserId}?api_key=${MY_KEY}`)
        .then((result)=>{
            navigate(`/InfoDisplay`,{state:result.data})
        })
        .catch((error)=>{
            alert(error.message);
        });
    }

    function RandomHandler(){
        axios.get(`https://api.nasa.gov/neo/rest/v1/neo/browse?api_key=${MY_KEY}`)
        .then((resp)=>{
            console.log(resp.data);
            var tempData= resp.data.near_earth_objects;
            var randomId=Math.floor(Math.random() * 20);
            submitHandler(tempData[randomId].id);
        })
        .catch((error)=>{
            console.log("Error is "+error.message);
            alert(error.message);
        })
        
    }


    return(
        <div className={classes.root} id="HomeMain">
            <h3>This is Asteroid App</h3>

            <TextField  label="Asteroid ID" value={UserId} placeholder='Enter Asteroid ID' variant="filled" color="success" 
            onChange={(e)=>{setUserId(e.target.value)}} focused />
            <div className='btn-flex'>
                  <Button  variant="contained" color="success" onClick={()=>{submitHandler(UserId)} } disabled={!UserId}>
                  Submit
                  </Button>
          
            <Button  variant="outlined" onClick={RandomHandler}>Random Asteroid</Button>
        
           <Link to="/Demo"><Button>Click</Button></Link>
            </div>

        </div>
    );
}



