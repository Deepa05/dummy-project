import Button from '@mui/material/Button';
import { styled } from '@mui/styles';

const MyButton = styled(Button)({
    color:"red",
    background: 'violet'

})

export default function Demo(){

    return(
        <div>
            <h2>This is demo  app using styled Component</h2>
            <p>custom css gets applied for the first time mount & if we refresh the page it will get vanished & the default on will apply</p>
            <MyButton>Click</MyButton>
            {/* <MyButton  variant="contained" color="success">
                  Submit
                  </MyButton> */}

        </div>
    );
}




// import PropTypes from 'prop-types';
// import { withStyles } from '@mui/styles';
// import Button from '@mui/material/Button';

// const styles = {
//     root:{
//         background:'violet',
//         color:'red',
//         '&:hover':{background:'black'}
//     },
    
// };

//  function Demo(props){

//     const { classes } = props;

//     return(
//         <div>
//             <h2>This is demo  app using higher order Component</h2>
//             <p>custom css gets applied for the first time mount & if we refresh the page it will get vanished & the default on will apply</p>
//             <Button className={classes.root}>Click</Button>
//         </div>
//     );

// }

// Demo.propTypes = {
//     classes: PropTypes.object.isRequired,
//   };

// export default withStyles(styles)(Demo);


// import Button from '@mui/material/Button';
// import { makeStyles } from '@mui/styles';

// const useStyles = makeStyles({
//     root:{
//         color:"red",
//         background: 'violet',
//         '& p':{
//             color:'green'
//         },
//         '& Button':{
//             background:'blue',
//             color:'pink'
//         }
//     }
// })

// export default function Demo(){

//     const classes = useStyles();

//     return(
//         <div className={classes.root}>
//             <h2>This is demo  app using nesting selectors</h2>
//             <p>custom css gets applied for the first time mount & if we refresh the page it won't get vanished</p>
//             <Button className={classes.root} variant="contained" color="success">
//                   Submit
//             </Button>

//         </div>
//     );
// }

