
import './App.css';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';
import HomePage from './HomePage';
import InfoDisplay from './InfoDisplay';
import Demo from './Demo';

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path='/' element={<HomePage/>}></Route>
          <Route path='/InfoDisplay' element={<InfoDisplay/>}></Route>
          <Route path='/Demo' element={<Demo></Demo>}></Route>
        </Routes>
      </Router>
      
    </div>
  );
}

export default App;
